FROM node

ADD . /stc-frontend
WORKDIR /stc-frontend
RUN npm install

CMD ["npm", "run", "build"]
