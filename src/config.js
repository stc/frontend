const config = {
    controller: process.env.CONTROLLER_ADDRESS || "http://localhost:5000"
};

module.exports = config;
