import React, {Component} from 'react';


function takep(width, height) {
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var photo = document.getElementById('photo');

    var context = canvas.getContext('2d');
    if (width && height) {
        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);

        photo.setAttribute('src', canvas.toDataURL('image/png'));

        console.log(photo.getAttribute('src'))
    }
}

let Camera = React.createClass({

    photoTaken: function () {
        this.setState({
            firstPhotoTaken: true
        })
    },

    getInitialState: function () {
        return {
            firstPhotoTaken: false, // TODO: temporary for presentation purpose

            first: this.props.isFirst,
            renderCanvas: false
        }
    },

    componentDidMount: function () {

        var width = 320;    // We will scale the photo width to this
        var height = 0;     // This will be computed based on the input stream

        // |streaming| indicates whether or not we're currently streaming
        // video from the camera. Obviously, we start at false.

        var streaming = false;

        // The various HTML elements we need to configure or control. These
        // will be set by the startup() function.

        var video = null;
        var canvas = null;
        var photo = null;
        var startbutton = null;

        function startup() {
            video = document.getElementById('video');
            canvas = document.getElementById('canvas');
            photo = document.getElementById('photo');
            startbutton = document.getElementById('startbutton');

            navigator.getMedia = ( navigator.getUserMedia ||
                navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia ||
                navigator.msGetUserMedia);

            navigator.getMedia(
                {
                    video: {
                        facingMode: {exact: "environment"}
                    },
                    audio: false,

                },
                function (stream) {
                    if (navigator.mozGetUserMedia) {
                        video.mozSrcObject = stream;
                    } else {
                        var vendorURL = window.URL || window.webkitURL;
                        video.src = vendorURL.createObjectURL(stream);
                    }
                    video.play();
                },
                function (err) {
                    console.log("An error occured! " + err);
                }
            );

            video.addEventListener('canplay', function (ev) {
                if (!streaming) {
                    height = video.videoHeight / (video.videoWidth / width);

                    // Firefox currently has a bug where the height can't be read from
                    // the video, so we will make assumptions if this happens.

                    if (isNaN(height)) {
                        height = width / (4 / 3);
                    }

                    video.setAttribute('width', width);
                    video.setAttribute('height', height);
                    canvas.setAttribute('width', width);
                    canvas.setAttribute('height', height);
                    streaming = true;
                }
            }, false);

            startbutton.addEventListener('click', function (ev) {
                takep(width, height)
                ev.preventDefault();
            }, false);

        }

        // Fill the photo with an indication that none has been
        // captured.


        // Capture a photo by fetching the current contents of the video
        // and drawing it into a canvas, then converting that to a PNG
        // format data URL. By drawing it on an offscreen canvas and then
        // drawing that to the screen, we can change its size and/or apply
        // other changes before drawing it.


        // Set up our event listener to run the startup process
        // once loading is complete.
        window.addEventListener('load', startup, false);
        // load photos
    },

    render: function () {
        var canvasStyle = !this.state.renderCanvas ? {
            // TODO: clean up this shit
            width: 0,
            height: 0
        } : {
            display: "block"
        };
        return (
            <div>
                <div className="camera"
                     style={this.state.style}>
                    <button
                        id="startbutton"
                        onClick={this.photoTaken}
                    >
                        <video id="video">Video stream not available.</video>
                    </button>
                </div>
                <canvas id="canvas" style={canvasStyle}>
                </canvas>
            </div>
        )
    }
});


export default Camera
