import React, {Component} from 'react';
import Place from './place/Place'
import './App.css';

let Infinite = require('react-infinite');


class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="Header">
                    <h1>Spacetime Colider</h1>
                </div>
                <Infinite useWindowAsScrollContainer elementHeight={40}>
                    <Place
                        isFirst="true"
                    />
                    <Place/>
                    <Place/>
                    <Place/>
                    <Place/>
                    <Place/>
                </Infinite>
            </div>
        );
    }
}

export default App;
