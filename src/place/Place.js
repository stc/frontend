import React, {
  Component
} from 'react';
import './Place.css'
import Camera from '../camera/Camera'
import axios from 'axios'
import config from '../config'


let Place = React.createClass({
  getInitialState() {
    return {
      location: null,
      style: {
        overflow: "hidden"
      },
      error: null,
      images: [],
      first: this.props.isFirst,
      renderCanvas: false
    }
  },

  async componentWillMount() {
    await this.fetchImages()
  },

  handleScroll() {
    if (this.props.isFirst) {
      this.setState({
        style: {
          overflow: "initial"
        },
      })
    }
  },

  async fetchImages() {
    try {
      console.log('fetchign images...')
      const res = await axios.get(config.controller)
      console.log(res.data)
      this.setState({
        images: res.data[0].images,
      })
    } catch (e) {
      this.setState({
        error: 'Request failed...',
      })
      console.error(e)
    }
  },

  render() {
    return (
      <div onScroll={this.handleScroll} className = "Place" >
      {this.state.first ? <Camera/> : null}
      {this.state.error ? <p>{this.state.error}</p> : null}
      {!this.state.images
        ? <p>spinner</p>
        : this.state.images.map(image =>
          <img className="PlacePhoto" src={config.controller + image.url} />)
      }
      </div>
    )
  },
});

export default Place
